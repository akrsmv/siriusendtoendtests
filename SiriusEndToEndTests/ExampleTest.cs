using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;

namespace SeleniumTests
{
    [TestFixture]
    public class ExampleTest
    {
	private ISelenium selenium;

	/// <summary>
	/// This method is to be executed BEFORE the actual test start (initializing selenium related stuff)
	/// </summary>
	[SetUp]
	public void SetupTest()
	{
	    ChromeOptions options = new ChromeOptions();
	    options.AddArgument("--start-maximized");
	    var driver = new ChromeDriver(options);
	    selenium = new WebDriverBackedSelenium(driver, "http://sirius.circlesit.net/");
	    selenium.Start();
	}

	/// <summary>
	/// This method is to be executed AFTER the actual test finishes (cleaning up, stopping selenium related stuff)
	/// </summary>
	[TearDown]
	public void TeardownTest()
	{
	    try
	    {
		selenium.Stop();
	    }
	    catch (Exception)
	    {
		// Ignore errors if unable to close the browser
	    }
	}

	/// <summary>
	///  the name of the test doesnt really matters to the compiler. 
	///  It is the [Test] attribute that makes it actually a test. The name should be meaninggull to developers
	///  something like LoginIn_GoToAdministration_CreateUser_LogOut()
	///  it is a common practice for test methods to have loong names - they must say what the test is doing
	/// </summary>
	[Test]
	public void TheTest()
	{
	    selenium.Open("/");
	    Thread.Sleep(3000); // couldnt find a better way to force selenium to wait for the page to load (there is a dedicated method for that but couldnt)
	    selenium.WindowMaximize(); // make the browser full screen so you can see what happens
	    selenium.SetSpeed("500"); // this will set the delay speed so the automated test can be visually observed. Better without it for performance, but in the begginig its good to see what happens
	    // login
	    selenium.Type("id=username-input", "see.ceo");
	    selenium.Type("id=password-input", "123");
	    selenium.Click("id=sign-in-button");
	    selenium.WaitForPageToLoad("3000");

	    // walk around in the menus
	    selenium.Click("link=Administration");
	    selenium.Click("id=staff-menu");
	    // when the page in testing contains some grid results, the below statement will wait until the results are populated in the gird table (if there are no results, currently it will hang for 5 seconds)
	    // staff page is such example - it loads asynchronously the staff via ajax calls (i.e the page appears loaded, but there are some javascript calls that further manipulate the DOM)
	    selenium.WaitForCondition("selenium.browserbot.getCurrentWindow().document.querySelectorAll(\"div.ag-body-container > div\").length > 0", "5000");

	    selenium.Click("link=Administration");
	    selenium.Click("id=places-menu");
	    selenium.WaitForPageToLoad("3000");// wait for places page to load (so this is the dedicated method for waiting instead of Thread.Sleep but it works only after some call to selenium.click ?)

	    // then logout
	    selenium.Click("id=user-context-menu");
	    selenium.Click("id=sign-out-button");
	    selenium.WaitForPageToLoad("3000");

	    // TODO Assert stuff in order to proove successful test
	    
	}
    }
}
